//http://html5demos.com/file-api

var holder = document.getElementById('holder');

holder.ondragover = function () { 
   this.className = 'hover'; 
   document.getElementById('holder-status').innerHTML = 'Release your file';
   return false; 
}

holder.ondragend = function () { this.className = ''; return false; };

holder.ondrop = function (e) {
  this.className = '';
  e.preventDefault();
  this.className = 'loading'; 
   document.getElementById('holder-status').innerHTML = 'Loading';
  var file = e.dataTransfer.files[0];
  var reader = new FileReader();
  
  //reader.onload = load_binary
  //reader.readAsDataURL(file);
  
  if(window.location.hash) {
      reader.onload = load_doge;
  } else {
      reader.onload = load_base64;
  }
  
  reader.readAsDataURL(file);
  
  return false;
}

function load_doge(event){
    console.log("load doge");
  var base64 = event.target.result;
  echonestIdentify(base64, "song_type");
}

function load_base64(event){
console.log("load game");
  var base64 = event.target.result;
  echonestIdentify(base64, "audio_summary");

}

