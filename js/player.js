var playlist;
var jplayer;
var currentSong ;
loadPlaylist();
function checkIfSongHasChanged(){
    if (playlist.playlist.length > 0 ){
        $("#song_info_button").attr("disabled", false);
        var current=playlist.current;
        var newSong = playlist.playlist[current].mp3;
        if (newSong != currentSong){
            currentSong = newSong;
            updateSong();
        }
        //check if the playlist is stuck because a song is broken
        if (currentSongIsBroken()){
            console.log("The song "+newSong+" is broken!!");
            playlist.next();
        }
    } else {
        $("#song_info_button").attr("disabled", true);
        $("#song_info_panel").slideUp();
    }
}
function currentSongIsBroken(){
 return jplayer.status.duration == 0;   
}
function updateSong(){
    var songName = filename(currentSong);
    var fullPath = getFolder(currentSong).replace('folders/','');
    var folder = getLastFolder(currentSong);
    notification(null, folder, songName);
    saveWoal();

    $("#song_folder").text(fullPath);
    $("#song_folder").click(function(){ls(fullPath); });
}
function loadPlaylist(){
    //jplayer demo http://www.jplayer.org/latest/demo-02-jPlayerPlaylist/
    playlist = new jPlayerPlaylist({
            jPlayer: "#jquery_jplayer_1",
            cssSelectorAncestor: "#jp_container_1"
        }, 
        [],
        {
            playlistOptions: {
                enableRemoveControls: true,
                smoothPlayBar: true,
                keyEnabled: true
            },
            //swfPath: "jplayer",
             //http://www.longtailvideo.com/support/jw-player/28836/media-format-support/
            supplied: "mp3, oga", //,m4v,flv
            wmode: "window"
        });
}


function addSong(title,song){
    console.log("Playing song "+title+" in path "+song);
    title = title.replace(/_/g,' ');
    playlist.add({
        title:title,
        mp3:song,
        oga:song
    }, true);
    
}


function clearPlaylist(){
    playlist.remove();
}
