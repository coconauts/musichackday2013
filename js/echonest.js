var uploadURL = "http://developer.echonest.com/api/v4/track/upload";
var api_key = "SZ5GCI2ZRHEV70RUQ";

var title ;
var filepath ; 
function echonestUploadRemote(path){
    $.post(uploadURL,
        {   api_key: api_key, 
            url: path
        } 
    )
    .done(function(msg){echonestProcessor(msg)});
    
}

function echonestIdentify( base64, bucket){
    var formData = {"base64": base64, "bucket": bucket}; //Array 
     $.ajax({
         url :"php/echonestIdentify.php",
         type: "POST",
         data : formData
    })
    .done(function(msg){
        if (bucket == "audio_summary") {
            echonestProcessor(msg);
        } else {
            echonestProcessorDoge(msg);
        }
    });
}


function echonestProcessor(response){
    console.log(response);
    
    var json = JSON.parse(response);
    var file = json.file;
    
    var info = JSON.parse(json.info);
    
    if (info.response.status.code != 0 || info.response.songs.length == 0 ) {
       document.getElementById('holder').className = ''; 
        document.getElementById('holder-status').innerHTML = 'Unable to recognise';   
    } else {
        var song = info.response.songs[0];
        var id =  song.id;
        title = song.title ;
        filepath=  json.file;
        
        showInfo(title);
        
        echonestImage(id);
        echonestAnalysis(song.audio_summary.analysis_url);
    }  
}


function echonestProcessorDoge(response){
    console.log(response);
    
    var json = JSON.parse(response);
    var file = json.file;
    
    var info = JSON.parse(json.info);
    
    if (info.response.status.code != 0 || info.response.songs.length == 0 ) {
       document.getElementById('holder').className = ''; 
        document.getElementById('holder-status').innerHTML = 'Unable to recognise';   
    } else {
        var song = info.response.songs[0];
        var id =  song.id;
        title = song.title ;
        filepath=  json.file;
        
        makeAttribs(song.song_type);

    }  
}

function echonestAnalysis(url){
   
   console.log("analysisUrl "+url);
   $.ajax(url)
   .done(function(response){ updateAnalysis(response); });
}

function echonestImage(id){

    var imageUrl = "http://developer.echonest.com/api/v4/track/profile?api_key="+api_key+"&format=json&id="+id+"&bucket=audio_summary";
   
   console.log("imageUrl "+imageUrl);
   $.ajax({
       url: imageUrl,
       type: "GET",
       crossDomain: true,
       dataType: "text"
   })
   .done(function(response){ updateImage(response); });
}
