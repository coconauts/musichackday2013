var dogeAttrs;
var backgroundImage = new Image(); 
function dogeAttr(){
    var x = 0 ;
    var y = 0;
    var txt = "wow";
    var color = "#f00";
    
}

function init(){
    
backgroundImage.src = 'images/doge.jpg'; 
 canvas = document.getElementById('canvasDoge');
    ctx = canvas.getContext('2d');
    ctx.font = "bold 24px comic sans MS";   
}
function random(from, to){
 return Math.floor((Math.random()*to)+from);   
}


function addAttrib(attr_txt, attr_i){
    console.log("Adding doge attr: "+attr_i);
    var newId = dogeAttrs.length;
    dogeAttrs[newId] = new dogeAttr();
//    dogeAttrs[newId].x = random(0,300);
//    dogeAttrs[newId].y =random(20,400);
    setRandomPos(attr_i, dogeAttrs[newId]);
    dogeAttrs[newId].txt = attr_txt;
    dogeAttrs[newId].color = randomColor();
}

function setRandomPos(i, attr){
    if (i==0) {attr.x = 30; attr.y = 50;}
    else if (i==1) {attr.x = 200; attr.y = 370;}
    else if (i==2) {attr.x = 30; attr.y = 300;}
    else if (i==3) {attr.x = 300; attr.y = 100; }
    else {
        attr.x = random(0,300);
        attr.y = random (20,400);
    }
    
    console.log("Posotion for attr_"+i+": "+attr.x+","+attr.y);
}


function randomColor(){
    var r = random(0,5);
    switch (r){
        case 0: return "#f00";
        case 1: return  "#0f0";
        case 2: return "#00f";
        case 3: return "#f0f";
        case 4: return "#ff0";
        case 5: return "#0ff";
        default: return "#000";
    }
    
}

function drawDoge(){
    ctx.drawImage(backgroundImage,0,0,500,500);
    for (var i = 0; i < dogeAttrs.length; i++){
         var attr = dogeAttrs[i];
         ctx.fillStyle = attr.color; 
         ctx.fillText(attr.txt, attr.x , attr.y);
    }
}
init();
