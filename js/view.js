function showInfo(title){
    
   $('#holder').slideUp();
   $('#info').slideDown();
   
   $('#title').html(title);
   
}

function restart(){
    document.getElementById('holder').className = ''; 
    document.getElementById('holder-status').innerHTML = 'Drop your music here';   

    $('#holder').slideDown();
    $('#info').slideUp();
    $('#doge').slideUp();
    clearPlaylist();
}
function updateImage(response){
    console.log("update image "+response);
    var json = JSON.parse(response);
    var img = json.response.track.release_image;
    if (img!= null) $('#cover').attr("src",img);
    else $('#cover').attr("src","css/images/no_image_available.png");
}

function makeAttribs(song_types){
    dogeAttrs = new Array();
    for (i=0; i< song_types.length  ; i++){
        addAttrib(randomPrefix()+song_types[i], i); 
    }
    addAttrib("wow", song_types.length);
    drawDoge();
    
    $('#holder').slideUp();
   $('#doge').slideDown();
}
function randomPrefix(){
    var r = random(0,5);
    switch (r){
        case 0: return "so ";
        case 1: return "much ";
        case 2: return "many ";
        case 3: return "very ";
        case 4: return "such ";
        default: return "so ";
    }
}

function updateAnalysis(json){
    start();
 
    for (var seg_i = 0; seg_i < json.segments.length; seg_i++) {
        var segment = json.segments[seg_i];
        //var selected_pitches = new array();
        for (var pitch_i = 0 ; pitch_i<12 ; pitch_i++){
            pitch_val = segment.pitches[pitch_i];
            if (pitch_val>0.8){
                addEnemy(segment.start, pitch_i);
                // Potential candidate. Check it's stronger than neighbors.
//                prev_i = pitch_id-1;
//                if (prev_i<0) prev_i = 11;
//                next_i = (pitch_id+1)%12;
//                if (pitch_val > segment.pitches[prev_i] 
//                && pitch_val > segment.pitches[next_i]){
//                    addEnemy(segment.start, pitch_i);
//                }
            }
        }
    }
    setTimeout(function(){addSong(title,filepath)},2000);
}

