//javascript keycodes
//http://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes
var img = new Image();
var imgEnemy = new Image();
var ctx;
var cursorx;
var cursory;

var canvas;
var initialized = false;
var score = 0;

var stars;
function star(){
    this.x = 0;
    this.y = 0;
}

var player;
function player(){
    this.x = 30;
    this.y =250;
}
var enemies;
function enemy (){
    this.active =true;
    this.x =0;
    this.y =0;
    this.time = 0;
    this.pitch = "A";
}

/*CONTROLLERS*/
function onMouseMove(evt) {
    cursorx = evt.pageX ;
    cursory = evt.pageY ;

    cursorx -= canvas.offsetLeft;
    cursory -= canvas.offsetTop;
}

function start(){
    if (!initialized) init();
    else reset();
    
}
function init(){
    
    window.addEventListener("mousemove", onMouseMove, false);

    img.src = 'images/ship.png';
    imgEnemy.src = "images/music.png";
    canvas = document.getElementById('canvas');
   
    ctx = canvas.getContext('2d');
    setInterval(loop,50);
    ctx.font = "bold 24px verdana, sans-serif ";
    ctx.fillStyle = "#ffffff"; 
    ctx.strokeStyle = "#ffffff";
    
    player= new player();
    enemies= new Array();
    
    loadStars();
    initialized = true;
}
function reset(){
    enemies= new Array();
}
function loop() {
    update();
    draw();
}

function draw(){
    ctx.clearRect(0,0,500,500); 
    
    for (var i = 0; i < stars.length; i++){
        var star = stars[i];
        ctx.strokeRect(star.x,star.y,1,1);       
    }
    
    ctx.drawImage(img, player.x-30, player.y-20,64,41);
    
    for (var i = 0; i < enemies.length; i++){
        var enemy = enemies[i];
        if (enemy.active) {
            ctx.drawImage(imgEnemy, enemy.x , enemy.y,45,45);
            ctx.fillText(enemies[i].pitch, enemies[i].x , enemies[i].y);
        }
    }
    
    ctx.fillText("Score: "+score, 20, 480);
}

function update(){
    updateShip();
    updateEnemies();
    updateStars();
}

function updateShip(){
    
    if (player.y < 0 ) player.y = 0;
    if (player.y > 500) player.y = 500;
           
    //x=Math.round((x+(cursorx-x)/5));
    if (isNaN(player.y) ) player.y = 0;
    player.y=Math.round((player.y+(cursory-player.y)/5));
    
}

function collision(enemy, player){
    if (enemy.x < 50 && enemy.active){
        if (player.y  > enemy.y -20 && player.y < enemy.y + 60 ) {
            score++;
            enemy.active = false;
        }
    }
}
function updateEnemies(){
    for (var i = 0; i < enemies.length; i++){
         var enemy = enemies[i];
         if (enemy.active) {
             collision(enemy,player);
             enemy.x -= 250/20;
         }
         else if( enemy.x < 0 ) enemy.active = false;
    }
}
function updateStars(){
    for (var i = 0; i < stars.length; i++){
         var star = stars[i];
         if (star.x > 0 )  star.x -= 250/40;
         else star.x = 500;
    }
}
function addEnemy(time, pitch) {
    
    //console.log("addEnemy("+time+","+pitch+");");
    var newId = enemies.length;
    enemies[newId] = new enemy();
    enemies[newId].x = 500+250*time;
    enemies[newId].y = pitch * 450/12;
    enemies[newId].time = time;
    enemies[newId].active = true;
    enemies[newId].pitch = pitch2letter(pitch);
}

function pitch2letter(pitch){
    
        switch(pitch){
            
            case 0: return "C";
            case 1: return "C#";
            case 2: return "E";
            case 3: return "E#";
            case 4: return "F";
            case 5: return "F#";
            case 6: return "G";
            case 7: return "G#";
            case 8: return "A";
            case 9: return "A#";
            case 10: return "B";
            case 11: return "B#";
        }
}
function random(from, to){
 return Math.floor((Math.random()*to)+from);   
}
function loadStars(){
    stars = new Array();
    for (var i = 0; i< 200;i++){
        stars[i] = new star();
        stars[i].x = random(0, 500);
        stars[i].y = random(0, 500);
    }
}