# Spotify

http://musicmachinery.com/2011/12/02/building-a-spotify-app/

# Musixmatch

API key: eb779f8b69382e6d033c28abc74b0ec1


# echonest

API key: SZ5GCI2ZRHEV70RUQ

# Info

http://developer.echonest.com/raw_tutorials/faqs/faq_03.html
http://notes.variogr.am/post/27796385927/the-audio-fingerprinting-at-the-echo-nest-faq
http://developer.echonest.com/docs/v4/_static/AnalyzeDocumentation.pdf
http://musicmachinery.com/2012/04/09/syncing-echo-nest-analysis-to-spotify-playback/
http://musicmachinery.com/2009/03/19/overview-of-the-echo-nest-remix-api/
http://echonest.github.io/remix/
## Curl

Using local song path:

    marbu > curl -F "api_key=SZ5GCI2ZRHEV70RUQ" -F "filetype=mp3" -F "track=@magic.mp3" "http://developer.echonest.com/api/v4/track/upload"
{"response": {"status": {"version": "4.2", "code": 0, "message": "Success"}, "track": {"status": "pending", "song_id": "SOMXIOM12AB018122B", "title": "A Kind Of Magic", "artist": "Queen", "id": "TRYXQUJ142B91C77D6", "analyzer_version": "3.2.2", "release": "A Kind Of Magic", "artist_id": "ARL4TII1187B9B46E1", "bitrate": 128, "audio_md5": "e1383bc91e568e7b697fd7fff5d80362", "samplerate": 44100, "md5": "e1383bc91e568e7b697fd7fff5d80362"}}}[01:07:12]

Using file md5 hash:

    marbu > curl "http://developer.echonest.com/api/v4/track/profile?api_key=SZ5GCI2ZRHEV70RUQ&format=json&md5=e1383bc91e568e7b697fd7fff5d80362&bucket=audio_summary"
{"response": {"status": {"version": "4.2", "code": 0, "message": "Success"}, "track": {"status": "complete", "song_id": "SOMXIOM12AB018122B", "audio_md5": "e1383bc91e568e7b697fd7fff5d80362", "artist": "Queen", "title": "A Kind Of Magic", "analyzer_version": "3.2.2", "audio_summary": {"time_signature": 0, "tempo": 0.0, "energy": 0.69913120814972329, "liveness": 0.60955210660644177, "analysis_url": "http://echonest-analysis.s3.amazonaws.com/TR/TRYXQUJ142B91C77D6/3/full.json?AWSAccessKeyId=AKIAJRDFEY23UEVW42BQ&Expires=1386423030&Signature=XR6c6n/fiM34CacARnxdARPbfvU%3D", "speechiness": 0.072840651973135218, "acousticness": 0.48393197680280858, "danceability": 0.0, "key": 6, "duration": 9.9004100000000008, "loudness": -13.634, "valence": 0.0, "mode": 0}, "release": "A Kind Of Magic", "artist_id": "ARL4TII1187B9B46E1", "bitrate": 128, "id": "TRYXQUJ142B91C77D6", "samplerate": 44100, "md5": "e1383bc91e568e7b697fd7fff5d80362"}}}[01:20:30]

## Pyechonest

Preparation:

    from pyechonest import config
    config.ECHO_NEST_API_KEY="SZ5GCI2ZRHEV70RUQ"
    from pyechonest import track    


Using local song path:

    songinfo = track.track_from_filename("music/magic.mp3")

Using md5 hash:

    songinfo = track.track_from_md5("e1383bc91e568e7b697fd7fff5d80362")

In both cases, the contents of songinfo should be something like:

    {u'status': u'complete', u'liveness': 0.6095521066064418, u'tempo': 0.0, u'energy': 0.6991312081497233, 'analysis_url': u'http://echonest-analysis.s3.amazonaws.com/TR/TRYXQUJ142B91C77D6/3/full.json?AWSAccessKeyId=AKIAJRDFEY23UEVW42BQ&Expires=1386425266&Signature=MXw2asteMA6m6mvrBZnCZg2AqhM%3D', u'speechiness': 0.07284065197313522, u'key': 6, u'duration': 9.90041, u'samplerate': 44100, u'bitrate': 128, 'id': u'TRYXQUJ142B91C77D6', 'md5': u'e1383bc91e568e7b697fd7fff5d80362', u'song_id': u'SOMXIOM12AB018122B', u'valence': 0.0, u'audio_md5': u'e1383bc91e568e7b697fd7fff5d80362', u'title': u'A Kind Of Magic', '_object_type': 'track', 'cache': {}, u'acousticness': 0.4839319768028086, u'analyzer_version': u'3.2.2', u'danceability': 0.0, u'time_signature': 0, u'release': u'A Kind Of Magic', u'loudness': -13.634, u'artist_id': u'ARL4TII1187B9B46E1', u'artist': u'Queen', u'mode': 0}     
    

    
    

    
